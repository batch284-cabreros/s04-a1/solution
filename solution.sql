-- Activity Instuctions:
-- Create an solution.sql file inside s04/a1 project and do the following using the music_db database:

-- - Find all artists that has letter d in its name.

USE music_db;

SELECT * FROM artists WHERE artists.name LIKE "%d%";



-- - Find all songs that has a length of less than 3:50.

USE music_db;


SELECT * FROM songs WHERE length < '00:03:50';


-- -Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)

USE music_db;


SELECT albums.album_title, songs.song_name, songs.length FROM albums 
	INNER JOIN songs ON albums.id = songs.album_id;


-- -Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)

USE music_db;


SELECT albums.album_title FROM artists
	INNER JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE '%a%';


-- -Sort the albums in Z-A order. (Show only the first 4 records.)

USE music_db;


SELECT * FROM albums
	ORDER BY album_title DESC LIMIT 4;


-- -Join the 'albums' and 'songs' tables. (Sort albums from Z-A)

USE music_db;


SELECT * FROM albums
	INNER JOIN songs ON albums.id = songs.album_id ORDER BY albums.album_title DESC